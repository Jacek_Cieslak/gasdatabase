using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;


namespace GasDatabase
{
    class ConvCalculator
    {
        enum Direction
        {
            ROW,
            COLUMN
        }
        public static List<ConvlutionGasSeries> CalculateConvolutionForAll(ConvolutionParams parameters)
        {
            List<ConvlutionGasSeries> output = new List<ConvlutionGasSeries>();
            double waveNo1 = 1e7 / parameters.minWaveLength_nm;
            double waveNo2 = 1e7 / parameters.maxWaveLength_nm;
            int i = 0;
            foreach (ConvlutionGasData gas in parameters.gasData)
            {
                var singleConv = CalculateSingleConvolution(gas, parameters.PPI, parameters.CL, waveNo1, waveNo2);
                output.Add(new ConvlutionGasSeries(gas.gasData, singleConv, i));
                i++;
            }
            return output;
        }

        static private List<Point> CalculateSingleConvolution(ConvlutionGasData gas, int PPI, double CL, double waveNo1, double waveNo2)
        {
            List<Point> tfph = gas.gasData.data.FindAll(p => p.X > waveNo2 && p.X < waveNo1);
            if(tfph.Count == 0)
            {
                return new List<Point>();
            }
            double concentration = gas.GC;

            tfph = tfph.Select(p => { p.Y = Math.Pow(10, -1 * p.Y * concentration * CL * Math.Log(10)); return p; }).ToList();

            var waveNumbers = Vector<double>.Build.DenseOfEnumerable(LinSpace(tfph.Min(p => p.X) + gas.MA, tfph.Max(p => p.X) - gas.MA, tfph.Count));
            var transmission = Vector<double>.Build.DenseOfEnumerable(tfph.Select(p => p.Y));
            var ppiVect = Vector<double>.Build.Dense(PPI, i => i+1);
            var theta = 2 * Math.PI / PPI * ppiVect;
            var sin_theta =  theta.PointwiseSin();
            var cos_2theta = (theta*2).PointwiseCos();
            int nr_of_wave_numbers_2f = waveNumbers.Count;

            var waveNumbersMat = Repmat(waveNumbers, PPI, Direction.ROW);
            var sin_thetaMat = Repmat(sin_theta, nr_of_wave_numbers_2f, Direction.COLUMN);
            var cos_2thetaMat = Repmat(cos_2theta, nr_of_wave_numbers_2f, Direction.COLUMN);

            var nu = waveNumbersMat + gas.MA * sin_thetaMat;
            var I_0 = Matrix<double>.Build.Dense(PPI, nr_of_wave_numbers_2f, 1);
            var cs = MathNet.Numerics.Interpolate.CubicSpline(waveNumbers, transmission);
            var T = Matrix<double>.Build.Dense(PPI, nr_of_wave_numbers_2f, (i, j) => cs.Interpolate(nu[i, j]));
            var I = I_0.PointwiseMultiply(T);

            var x_out = Vector<double>.Build.Dense(nr_of_wave_numbers_2f, (i => 1e7 / waveNumbers[i]));
            var y_out = -1 * (I.PointwiseMultiply(cos_2thetaMat)).ColumnSums() / PPI;

            List<Point> outputList = new List<Point>();
            for (int i = 0; i < nr_of_wave_numbers_2f; i++)
            {
                outputList.Add(new Point(x_out[i], y_out[i]));
            }

            return outputList;
        }
        private static IEnumerable<double> LinSpace(double start, double stop, int num, bool endpoint = true)
        {
            var result = new List<double>();
            if (num <= 0)
            {
                return result;
            }

            if (endpoint)
            {
                if (num == 1)
                {
                    return new List<double>() { start };
                }

                var step = (stop - start) / ((double)num - 1.0d);
                result = Arange(0, num).Select(v => (v * step) + start).ToList();
            }
            else
            {
                var step = (stop - start) / (double)num;
                result = Arange(0, num).Select(v => (v * step) + start).ToList();
            }

            return result;
        }

        public static IEnumerable<double> Arange(double start, int count)
        {
            return Enumerable.Range((int)start, count).Select(v => (double)v);
        }

        public static IEnumerable<double> Power(IEnumerable<double> exponents, double baseValue = 10.0d)
        {
            return exponents.Select(v => Math.Pow(baseValue, v));
        }

        private static Matrix<double> Repmat(Vector<double> vect, int no, Direction direction)
        {

            if(direction == Direction.COLUMN)
            {
                var mat = Matrix<double>.Build.DenseOfColumnMajor(vect.Count,1, vect);
                for (int i = 1; i < no; i++)
                {
                    mat = mat.InsertColumn(i, mat.Column(0));
                }
                return mat;
            }
            else
            {
                var mat = Matrix<double>.Build.DenseOfRowMajor(1, vect.Count, vect);
                for (int i = 1; i < no; i++)
                {
                    mat = mat.InsertRow(i, mat.Row(0));
                }
                return mat;
            }
        }
    }
   
}
