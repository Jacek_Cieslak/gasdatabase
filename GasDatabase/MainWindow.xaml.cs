﻿using System.Windows;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.Windows.Media;
using System.Collections.Generic;
using System;
using System.Reflection;
using OxyPlot;
using System.Windows.Controls;

namespace GasDatabase
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Database db;
        public GasPlot separatedPlot { get; private set; }

        public MainWindow()
        {
            DataContext = this;
            separatedPlot = new GasPlot();

            db = new Database();
            InitializeComponent();
            plt_separated.IsManipulationEnabled = false;
            plt_separated.PlotMargins = new Thickness(0,0,60,0);
            refreshLists();

            tb_temperature.Text = "20";
            tb_concentration.Text = "1000";
            tb_opticalPath.Text = "1.0";

            //AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);

            //this.MyModel.Series.Add(new FunctionSeries(Math.Cos, 0, 10, 0.1, "cos(x)"));
        }

        private void Pb_loadFromDir_Click(object sender, RoutedEventArgs e)
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.IsFolderPicker = true;
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                db.loadAllFromDirectory(dialog.FileName);
                refreshLists();
            }
        }

        private void Pb_loadFromFile_Click(object sender, RoutedEventArgs e)
        {
            loadFromFileWindow window = new loadFromFileWindow(db);
            window.ShowDialog();
            refreshLists();
        }

        private void Pb_SearchInDatabaseClick(object sender, RoutedEventArgs e)
        {
            bool isError = false;
            searchParams pars = readSearchParams(out isError);
            if (isError)
            {
                db.SearchResults.Clear();
            }
            else
            {
                db.searchInDatabase(pars);
            }
            refreshLists();
            refreshPlot();
        }

        private void refreshLists()
        {
            db.SearchByName(tb_DatabaseGasName.Text);
            lb_searchResult.ItemsSource = null;
            lb_searchResult.ItemsSource = db.SearchResults;
            lb_selectedGases.ItemsSource = null;
            lb_selectedGases.ItemsSource = db.SelectedSignals;
            lb_DatabaseAllGases.ItemsSource = null;
            lb_DatabaseAllGases.ItemsSource = db.SearchByNameResults;
        }
        private void refreshPlot()
        {
            bool isError = false;
            plotParams pars = readPlotParams(out isError);
            if(!isError)
            {
                
                plt_separated.Visibility = Visibility.Visible;
                plt_separated.Model = separatedPlot.GetModel();
                separatedPlot.updatePlot(pars);
            }
            else
            {
                plt_separated.Visibility = Visibility.Hidden;
            }
            
        }
        private searchParams readSearchParams(out bool isError)
        {
            var bc = new BrushConverter();
            isError = false;
            float minAbsorpcion, maxAbsorpcion, opticalLength;
            int concentration, minWaveLength, maxWaveLength;
            if (float.TryParse(tb_opticalPath.Text.Replace('.', ','), out opticalLength))
            {
                tb_opticalPath.Foreground = (Brush)bc.ConvertFrom("#FF000000");
            }
            else
            {
                    tb_opticalPath.Foreground = (Brush)bc.ConvertFrom("#FFFF0000");
                    isError = true;
            }
            if (int.TryParse(tb_concentration.Text, out concentration))
            {
                tb_concentration.Foreground = (Brush)bc.ConvertFrom("#FF000000");
            }
            else
            {
                tb_concentration.Foreground = (Brush)bc.ConvertFrom("#FFFF0000");
                isError = true;
            }
            if (int.TryParse(tb_maxWaveLen.Text, out maxWaveLength))
            {
                tb_maxWaveLen.Foreground = (Brush)bc.ConvertFrom("#FF000000");
            }
            else
            {
                maxWaveLength = int.MaxValue;
                if (!string.IsNullOrEmpty(tb_maxWaveLen.Text))
                {
                    tb_maxWaveLen.Foreground = (Brush)bc.ConvertFrom("#FFFF0000");
                    isError = true;
                }
            }
            if (int.TryParse(tb_minWaveLen.Text, out minWaveLength))
            {
                tb_minWaveLen.Foreground = (Brush)bc.ConvertFrom("#FF000000");
            }
            else
            {
                minWaveLength = 0;
                if (!string.IsNullOrEmpty(tb_minWaveLen.Text))
                {
                    tb_minWaveLen.Foreground = (Brush)bc.ConvertFrom("#FFFF0000");
                    isError = true;
                }
            }
            if (float.TryParse(tb_maxAbsorp.Text, out maxAbsorpcion))
            {
                tb_maxAbsorp.Foreground = (Brush)bc.ConvertFrom("#FF000000");
            }
            else
            {
                maxAbsorpcion = int.MaxValue;
                if (!string.IsNullOrEmpty(tb_maxAbsorp.Text))
                {
                    tb_maxAbsorp.Foreground = (Brush)bc.ConvertFrom("#FFFF0000");
                    isError = true;
                }
            }
            if (float.TryParse(tb_minAbsorp.Text, out minAbsorpcion))
            {
                tb_minAbsorp.Foreground = (Brush)bc.ConvertFrom("#FF000000");
            }
            else
            {
                minAbsorpcion = 0;
                if (!string.IsNullOrEmpty(tb_minAbsorp.Text))
                {
                    tb_minAbsorp.Foreground = (Brush)bc.ConvertFrom("#FFFF0000");
                    isError = true;
                }
            }
            if (int.TryParse(tb_temperature.Text, out int Temperature))
            {
                tb_temperature.Foreground = (Brush)bc.ConvertFrom("#FF000000");
            }
            else
            {
                tb_temperature.Foreground = (Brush)bc.ConvertFrom("#FFFF0000");
                isError = true;
            }

            searchParams pars = new searchParams(Temperature, concentration, opticalLength, new range(maxWaveLength, minWaveLength), new rangeFloat(maxAbsorpcion, minAbsorpcion));
            return pars;
        }
        private plotParams readPlotParams(out bool isError)
        {
            float opticalLength;
            int concentration;
            isError = false;
            if (!float.TryParse(tb_opticalPath.Text.Replace('.', ','), out opticalLength))
            {
                opticalLength = 1;
                isError = true;
            }
            if (!int.TryParse(tb_concentration.Text, out concentration))
            {
                concentration = 1000;
                isError = true;
            }
            return new plotParams(concentration, opticalLength);
        }
        private void Pb_addSelection_Click(object sender, RoutedEventArgs e)
        {
            int tab = tabC_mainTabC.SelectedIndex;
            GasAtTemperature selectedGas = null;
            bool isError = false;
            if (tab == 0)
            {
                selectedGas = getSearchSelectedSignal(out isError);
            }
            else if (tab == 1)
            {
                selectedGas = getDatabaseSelectedSignal(out isError);

            }
            if (!isError)
            {
                if (!db.ifOnSelected(selectedGas.getLabel))
                {
                    bool parseError = false;
                    plotParams pars = readPlotParams(out parseError);
                    if(!parseError && db.SelectedSignals.Count<20)
                    {
                        GasSeries gasSeries = separatedPlot.AddSeries(selectedGas, pars);
                        db.SelectedSignals.Add(gasSeries);
                        refreshPlot();
                    }
                }
            }

            //int selectedItemIndex = lb_searchResult.SelectedIndex;
            //if (selectedItemIndex < 0) return;

            //var movedItem = searchResults[selectedItemIndex];
            //searchResults.RemoveAt(selectedItemIndex);
            //selectedSignals.Add(movedItem);
            refreshLists();
        }

        private void Pb_deleteSelection_Click(object sender, RoutedEventArgs e)
        {
            int selectedItemIndex = lb_selectedGases.SelectedIndex;
            GasSeries selectedItem = (GasSeries)lb_selectedGases.SelectedItem;

            if (selectedItemIndex < 0) return;

            //var movedItem = selectedSignals[selectedItemIndex];
            db.SelectedSignals.RemoveAt(selectedItemIndex);
            //plot.
            separatedPlot.RemoveSeries(selectedItem);
            
            refreshLists();
            refreshPlot();
        }

        private void Lb_DatabaseAllGases_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var tempGas = lb_DatabaseAllGases.SelectedValue;
            if(tempGas == null)
            {
                return;
            }
            Gas selectedGas = (Gas)tempGas;
            lbl_databaseSelectedGasName.Content = selectedGas.Name;
            lb_DatabaseSelectedGasSignals.ItemsSource = selectedGas.gasData;
        }
        
        private GasAtTemperature getDatabaseSelectedSignal(out bool error)
        {
            int selectedSignalIndex = (lb_DatabaseSelectedGasSignals.SelectedIndex);
            if(selectedSignalIndex < 0)
            {
                error = true;
                return null;
            }
            error = false;
            GasAtTemperature selectedSignal = (GasAtTemperature)lb_DatabaseSelectedGasSignals.SelectedValue;
            return selectedSignal;
        }
        private GasAtTemperature getSearchSelectedSignal(out bool error)
        {
            int selectedSignalIndex = (lb_searchResult.SelectedIndex);
            if (selectedSignalIndex < 0)
            {
                error = true;
                return null;
            }
            error = false;
            GasAtTemperature selectedSignal = (GasAtTemperature)lb_searchResult.SelectedValue;
            return selectedSignal;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            int index = lb_selectedGases.SelectedIndex;
            GasSeries checkedGas = (GasSeries)((CheckBox)sender).DataContext;
            checkedGas.isEnabled = true;
            refreshPlot();
            lb_selectedGases.SelectedIndex = index;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            int index = lb_selectedGases.SelectedIndex;
            GasSeries checkedGas = (GasSeries)((CheckBox)sender).DataContext;
            checkedGas.isEnabled = false;
            refreshPlot();
            lb_selectedGases.SelectedIndex = index;
        }

        private void Tb_DatabaseGasName_TextChanged(object sender, TextChangedEventArgs e)
        {
           refreshLists();
        }

        private void Pb_calculateConvolution_Click(object sender, RoutedEventArgs e)
        {
            ConvolutionWindow window = new ConvolutionWindow(db.SelectedSignals);
            window.Show();
        }

        private void Pb_authors_Click(object sender, RoutedEventArgs e)
        {
            AuthorsWindow window = new AuthorsWindow();
            window.ShowDialog();
        }
    }
}
