﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GasDatabase
{

    public class GasSeries : OxyPlot.AreaSeries
    {
        public GasAtTemperature Value;
        private GasSeries gasSeries;

        public string getLabel { get { return Value.getLabel; } }
        public bool isEnabled { get; set; }
        public int SeriesColorSet { get; set; }

        public GasSeries(GasAtTemperature newGasSignal, plotParams pars, int colorSet)
        {
            Value = newGasSignal;
            isEnabled = true;
            SeriesColorSet = colorSet;
            Points = GasSeries.fromListofPoints(newGasSignal.data, pars);
            Points2.Clear();
            Points2.Add(new DataPoint(newGasSignal.data.First().X, 0));
            Points2.Add(new DataPoint(newGasSignal.data.Last().X, 0));
            MinX = Math.Min(newGasSignal.data.First().X, newGasSignal.data.Last().X);
            MaxX = Math.Max(newGasSignal.data.First().X, newGasSignal.data.Last().X);
            MinY = 0;
            LineStyle = OxyPlot.LineStyle.Solid;
            Color = GasPlot.ColorPalette.ElementAt(SeriesColorSet);
            Fill = GasPlot.ColorPalette.ElementAt(SeriesColorSet);
            Title = newGasSignal.getLabel;
            XAxisKey = "X";
            YAxisKey = "Y";
        }

        public GasSeries(GasSeries gasSeries)
        {
            this.gasSeries = gasSeries;
        }

        public static IList<DataPoint> fromListofPoints(List<Point> points, plotParams pars)
        {
            IList<DataPoint> newList = new List<DataPoint>();
            foreach (Point p in points)
            {
                newList.Add(new DataPoint(1e7/p.X, pars.OpticalPath * pars.Concentration*p.Y));
            }
            return newList;
        }
        public void UpdateSeries(plotParams pars)
        {
            Points = GasSeries.fromListofPoints(Value.data, pars);
            Points2.Clear();
            Points2.Add(new DataPoint(Value.data.First().X, 0));
            Points2.Add(new DataPoint(Value.data.Last().X, 0));
            MinX = Math.Min(Value.data.First().X, Value.data.Last().X);
            MaxX = Math.Max(Value.data.First().X, Value.data.Last().X);
            MinY = 0;
        }
    }
    public class GasPlot
    {
        public PlotModel GetModel()
        {
            PlotModel model = new PlotModel();
            model.Axes.Add(new LinearAxis()
            {
                ExtraGridlineStyle = LineStyle.Solid,

                Position = AxisPosition.Left,
                MinorTickSize = 0,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                IsPanEnabled = false,
                IsZoomEnabled = true,            
            });
            model.Axes.Add(new LinearAxis()
            {
                ExtraGridlineStyle = LineStyle.Solid,

                Position = AxisPosition.Bottom,
                MinorTickSize = 0,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.None,
                IsPanEnabled = false,
                IsZoomEnabled = true,
            });
            model.BoxThickness = 0;
            foreach (var series in plotSeries)
            {
                if (series.isEnabled)
                {
                    model.Series.Add(series);
                }
            }
            model.IsLegendOutsidePlotArea = true;
            model.LegendPosition = LegendPosition.TopLeft;
            model.PlotMargins = new OxyThickness(300, 30, 30, 20);
            return model;
        }

        List<GasSeries> plotSeries;
        public GasPlot()
        {
            plotSeries = new List<GasSeries>();
        }
        protected int GetFreeColorSet
        {
            get
            {
                int freeColor = 0;
                List<int> takenSeries = new List<int>();
                foreach (GasSeries series in plotSeries)
                {
                    takenSeries.Add(series.SeriesColorSet);
                }
                takenSeries.Sort();
                foreach (int i in takenSeries)
                {
                    if (i != freeColor) break;
                    else freeColor++;
                }
                return freeColor;
            }
        }
        public GasSeries AddSeries(GasAtTemperature gasSignal, plotParams pars)
        {
            GasSeries newSeries = new GasSeries(gasSignal, pars, GetFreeColorSet);
            plotSeries.Add(newSeries);
            return newSeries;
        }
        public void RemoveSeries(GasSeries gasSignal)
        {
            plotSeries.Remove(gasSignal);
        }
        public static readonly Dictionary<string, OxyColor> ColorDict = new Dictionary<string, OxyColor>
    {
        { "Red", OxyColor.FromUInt32(0xffe6194b) },
        { "Green", OxyColor.FromUInt32(0xff3cb44b) },
        { "Yellow", OxyColor.FromUInt32(0xffffe119) },
        { "Blue", OxyColor.FromUInt32(0xff4363d8) },
        { "Orange", OxyColor.FromUInt32(0xfff58231) },

        { "Purple", OxyColor.FromUInt32(0xff911eb4) },
        { "Cyan", OxyColor.FromUInt32(0xff46f0f0) },
        { "Magenta", OxyColor.FromUInt32(0xfff032e6) },
        { "Lime", OxyColor.FromUInt32(0xffbcf60c) },
        { "Pink", OxyColor.FromUInt32(0xffff94be) },

        { "Teal", OxyColor.FromUInt32(0xff008080) },
        { "Lavender", OxyColor.FromUInt32(0xffe6beff) },
        { "Brown", OxyColor.FromUInt32(0xff9a6324) },
        { "Carmine", OxyColor.FromUInt32(0xff841839) },
        { "Maroon", OxyColor.FromUInt32(0xff800000) },

        { "Mint", OxyColor.FromUInt32(0xffaaffc3) },
        { "Olive", OxyColor.FromUInt32(0xff808000) },
        { "DarkGreen", OxyColor.FromUInt32(0xff204619) },
        { "Navy", OxyColor.FromUInt32(0xff000075) },
        { "Grey", OxyColor.FromUInt32(0xff808080) },
    };
        public static List<OxyColor> ColorPalette
        {
            get
            {
                List<OxyColor> newColorPalette = new List<OxyColor>();
                foreach(var color in ColorDict)
                {
                    newColorPalette.Add(color.Value);
                }
                return newColorPalette;
            }
        }
           
        public void updatePlot(plotParams pars)
        {
            foreach(GasSeries series in plotSeries)
            {
                series.UpdateSeries(pars);
            }
        }
    }
    public class ConvlutionGasSeries : OxyPlot.LineSeries
    {
        public GasAtTemperature Value;
        public string getLabel { get { return Value.getLabel; } }
        public bool isEnabled { get; set; }
        public int SeriesColorSet { get; set; }
        public ConvlutionGasSeries(GasAtTemperature newGasSignal, List<Point> convPoints, int colorSet) 
        {
            Value = newGasSignal;
            isEnabled = true;
            SeriesColorSet = colorSet;
            Points = ConvlutionGasSeries.fromListofPoints(convPoints);
            MinX = Math.Min(newGasSignal.data.First().X, newGasSignal.data.Last().X);
            MaxX = Math.Max(newGasSignal.data.First().X, newGasSignal.data.Last().X);
            MinY = 0;
            LineStyle = OxyPlot.LineStyle.Solid;
            Color = GasPlot.ColorPalette.ElementAt(SeriesColorSet);
            Title = newGasSignal.getLabel;
            XAxisKey = "X";
            YAxisKey = "Y";
        }

        public new static IList<DataPoint> fromListofPoints(List<Point> points)
        {
            IList<DataPoint> newList = new List<DataPoint>();
            foreach (Point p in points)
            {
                newList.Add(new DataPoint(p.X, p.Y));
            }
            return newList;
        }
        public void UpdateSeries(List<Point> points)
        {
            Points = ConvlutionGasSeries.fromListofPoints(points);
            MinX = Math.Min(Value.data.First().X, Value.data.Last().X);
            MaxX = Math.Max(Value.data.First().X, Value.data.Last().X);
            MinY = 0;
        }
    }
    public class ConvGasPlot 
    {
        public ConvGasPlot()
        {
            plotSeries = new List<ConvlutionGasSeries>();
        }
        List<ConvlutionGasSeries> plotSeries;
        public static readonly Dictionary<string, OxyColor> ColorDict = new Dictionary<string, OxyColor>
    {
        { "Red", OxyColor.FromUInt32(0xffe6194b) },
        { "Green", OxyColor.FromUInt32(0xff3cb44b) },
        { "Yellow", OxyColor.FromUInt32(0xffffe119) },
        { "Blue", OxyColor.FromUInt32(0xff4363d8) },
        { "Orange", OxyColor.FromUInt32(0xfff58231) },

        { "Purple", OxyColor.FromUInt32(0xff911eb4) },
        { "Cyan", OxyColor.FromUInt32(0xff46f0f0) },
        { "Magenta", OxyColor.FromUInt32(0xfff032e6) },
        { "Lime", OxyColor.FromUInt32(0xffbcf60c) },
        { "Pink", OxyColor.FromUInt32(0xffff94be) },

        { "Teal", OxyColor.FromUInt32(0xff008080) },
        { "Lavender", OxyColor.FromUInt32(0xffe6beff) },
        { "Brown", OxyColor.FromUInt32(0xff9a6324) },
        { "Carmine", OxyColor.FromUInt32(0xff841839) },
        { "Maroon", OxyColor.FromUInt32(0xff800000) },

        { "Mint", OxyColor.FromUInt32(0xffaaffc3) },
        { "Olive", OxyColor.FromUInt32(0xff808000) },
        { "DarkGreen", OxyColor.FromUInt32(0xff204619) },
        { "Navy", OxyColor.FromUInt32(0xff000075) },
        { "Grey", OxyColor.FromUInt32(0xff808080) },
    };
        public static List<OxyColor> ColorPalette
        {
            get
            {
                List<OxyColor> newColorPalette = new List<OxyColor>();
                foreach (var color in ColorDict)
                {
                    newColorPalette.Add(color.Value);
                }
                return newColorPalette;
            }
        }
        public void AddSeries(ConvlutionGasSeries gasSeries)
        {
            gasSeries.SeriesColorSet = GetFreeColorSet;
            plotSeries.Add(gasSeries);
        }
        public void RemoveSeries(ConvlutionGasSeries gasSignal)
        {
            plotSeries.Remove(gasSignal);
        }

        protected int GetFreeColorSet
        {
            get
            {
                int freeColor = 0;
                List<int> takenSeries = new List<int>();
                foreach (ConvlutionGasSeries series in plotSeries)
                {
                    takenSeries.Add(series.SeriesColorSet);
                }
                takenSeries.Sort();
                foreach (int i in takenSeries)
                {
                    if (i != freeColor) break;
                    else freeColor++;
                }
                return freeColor;
            }
        }

        public PlotModel GetModel()
        {
            PlotModel model = new PlotModel();
            model.Axes.Add(new LinearAxis()
            {
                ExtraGridlineStyle = LineStyle.Solid,

                Position = AxisPosition.Left,
                MinorTickSize = 0,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                IsPanEnabled = false,
                IsZoomEnabled = true,
            });
            model.Axes.Add(new LinearAxis()
            {
                ExtraGridlineStyle = LineStyle.Solid,

                Position = AxisPosition.Bottom,
                MinorTickSize = 0,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.None,
                IsPanEnabled = false,
                IsZoomEnabled = true,
            });
            model.BoxThickness = 0;
            foreach (var series in plotSeries)
            {
                if (series.isEnabled)
                {
                    model.Series.Add(series);
                }
            }
            model.IsLegendOutsidePlotArea = true;
            model.LegendPosition = LegendPosition.TopLeft;
            model.PlotMargins = new OxyThickness(300, 30, 30, 20);
            return model;
        }
    }
}
