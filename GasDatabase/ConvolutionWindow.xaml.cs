﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GasDatabase
{
    /// <summary>
    /// Logika interakcji dla klasy ConvolutionWindow.xaml
    /// </summary>
    public struct ConvolutionParams
    {
        public int PPI;
        public double CL;
        public double minWaveLength_nm;
        public double maxWaveLength_nm;
        public List<ConvlutionGasData> gasData;
    }
    public class ConvlutionGasData
    {
        public GasAtTemperature gasData;
        public double GC { get; set; }//concentration;
        public double MA { get; set; }//modulation
        public bool isEnabled { get; set; }
        public ConvlutionGasData(GasSeries gasSeries)
        {
            gasData = gasSeries.Value;
            GC = 10;
            MA = 0.25;
            isEnabled = true;
        }
        public string getLabel { get { return gasData.getLabel; } }
    
    }

   
    public partial class ConvolutionWindow : Window
    {
        public List<ConvlutionGasData> mSelectedGases;
        public List<ConvlutionGasSeries> mCalculatedSeries;
        public uint mPPI { get; set; }
        public double mCL { get; set; }
        public double mMinWaveLength { get; set; }
        public double mMaxWaveLength { get; set; }
        public ConvGasPlot ConvolutionPlot { get; private set; }

        public ConvolutionWindow(List<GasSeries> selectedSeries)
        {
            updateGasList(selectedSeries);
            ConvolutionPlot = new ConvGasPlot();
            mPPI = 4;
            mCL = 1.0;
            mMinWaveLength = 8000;
            mMaxWaveLength = 8020;
            InitializeComponent();
            tb_ConvPPI.DataContext = this;
            tb_ConvCL.DataContext = this;
            tb_ConvMinLambda.DataContext = this;
            tb_ConvMaxLambda.DataContext = this;
            lb_convGases.ItemsSource = null;
            lb_convGases.ItemsSource = mSelectedGases;
            
        }

        private List<ConvlutionGasData> updateGasList(List<GasSeries> selectedSeries)
        {
            List<ConvlutionGasData> newGasData = new List<ConvlutionGasData>();
            foreach (GasSeries gas in selectedSeries)
            {
                newGasData.Add(new ConvlutionGasData(gas));
            }
            mSelectedGases = newGasData;
            return newGasData;
        }

        private void Pb_DrawConvPlot_Click(object sender, RoutedEventArgs e)
        {
            ConvolutionParams pars = new ConvolutionParams();

            pars.PPI = (int)mPPI;
            pars.CL = mCL;
            pars.maxWaveLength_nm = mMaxWaveLength;
            pars.minWaveLength_nm = mMinWaveLength;
            pars.gasData = mSelectedGases;
                mCalculatedSeries = ConvCalculator.CalculateConvolutionForAll(pars);
                ConvolutionPlot = new ConvGasPlot();
                foreach(var series in mCalculatedSeries)
                {
                    ConvolutionPlot.AddSeries(series);
                }
                refreshPlot();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            ConvlutionGasData checkedGas = (ConvlutionGasData)((CheckBox)sender).DataContext;
            int index = mSelectedGases.IndexOf(checkedGas);
            mCalculatedSeries.ElementAt(index).isEnabled = true;
            ConvolutionPlot.AddSeries(mCalculatedSeries.ElementAt(index));
            checkedGas.isEnabled = true;
            refreshPlot();
            lb_convGases.SelectedIndex = index;
        }
        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            //int index = lb_convGases.SelectedIndex;
            ConvlutionGasData checkedGas = (ConvlutionGasData)((CheckBox)sender).DataContext;
            int index = mSelectedGases.IndexOf(checkedGas);
            mCalculatedSeries.ElementAt(index).isEnabled = false;
            ConvolutionPlot.RemoveSeries(mCalculatedSeries.ElementAt(index));
            checkedGas.isEnabled = false;
            refreshPlot();
            lb_convGases.SelectedIndex = index;
        }
        private void refreshPlot()
        {
            bool isError = false;
            if (!isError)
            {

                plt_Conv.Visibility = Visibility.Visible;
                plt_Conv.Model = ConvolutionPlot.GetModel();
            }
            else
            {
                plt_Conv.Visibility = Visibility.Hidden;
            }

        }
    }
}
