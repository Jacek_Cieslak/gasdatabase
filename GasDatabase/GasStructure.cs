﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GasDatabase
{
    public struct range
    {
        public int High;
        public int Low;
        public range(int high, int low)
        {
            High = high;
            Low = low;
        }
    }
    public struct rangeFloat
    {
        public float High;
        public float Low;
        public rangeFloat(float high, float low)
        {
            High = high;
            Low = low;
        }
    }

    public struct rangeDouble
    {
        public double High;
        public double Low;
        public rangeDouble(double high, double low)
        {
            High = high;
            Low = low;
        }
    }
    public struct searchParams
    {
        public int Temperaure;
        public float OpticalPath;
        public float Concentration;
        public range WaveLength;
        public rangeFloat Absorption;
        public searchParams(int temperature, int concentration, float opticalPath, range wave, rangeFloat absorption)
        {
            Temperaure = temperature;
            Concentration = concentration;
            OpticalPath = opticalPath;
            WaveLength = wave;
            Absorption = absorption;
        }

    }
    public struct plotParams
    {
        public float OpticalPath;
        public int Concentration;
        public plotParams(int concentration, float opticalPath)
        {
            OpticalPath = opticalPath;
            Concentration = concentration;
        }

    }

    public class selectedSignalTemplate
    {
        public bool IsDisplayed { get { return IsDisplayed; } set {
                IsDisplayed = value;
            } }
        public GasAtTemperature Value;
        public selectedSignalTemplate(bool isDisplayed, GasAtTemperature value)
        {
            IsDisplayed = isDisplayed;
            Value = value;
        }

        public string getLabel { get { return Value.getLabel; } }
    }

    public class Database
    {
        public Dictionary<string, Gas> GasCollection;
        public List<GasAtTemperature> SearchResults { get; set; }
        public List<GasSeries> SelectedSignals { get; set; }
        public List<Gas> SearchByNameResults { get; set; }
        public bool loadAllFromDirectory(string directoryPath)
        {
            bool result = true;
            string[] files = Directory.GetFiles(directoryPath, "*.*", SearchOption.AllDirectories);
            foreach (string filePath in files)
            {
                if (filePath.EndsWith(".txt") || filePath.EndsWith(".TXT"))
                {
                    string gasName = Path.GetFileName(Path.GetDirectoryName(filePath));

                    bool localResult = loadGasFromFile(filePath, gasName);
                    if (!localResult) result = false;
                }
            }
            return result;
        }
        public bool loadGasFromFile(string path, string gasName)
        {
            bool result;
            if(GasCollection.ContainsKey(gasName))
            {
                Console.WriteLine(gasName + " exists, adding new characteristics");
                result = GasCollection[gasName].loadData(path);
            }
            else
            {
                Console.WriteLine(gasName + " created");
                Gas newGas = new Gas(gasName);
                result = newGas.loadData(path);
                GasCollection.Add(gasName, newGas);
            }
            return result;
        }
        public Database()
        {
            GasCollection = new Dictionary<string, Gas>();
            SearchResults = new List<GasAtTemperature>();
            SelectedSignals = new List<GasSeries>();
        }
        
        public List<GasAtTemperature> searchInDatabase(searchParams search)
        {
            List<GasAtTemperature> searchResults = new List<GasAtTemperature>();
            rangeFloat absorption = new rangeFloat();
            absorption.Low = search.Absorption.Low /( search.Concentration * search.OpticalPath);
            absorption.High = search.Absorption.High / (search.Concentration * search.OpticalPath);
            foreach (var gas in GasCollection)
            {
                    GasAtTemperature bestSignal = gas.Value.getBestSignal(search.Temperaure);

                    if(bestSignal.IfContainsPointInBox(absorption, search.WaveLength))
                    {
                        searchResults.Add(bestSignal);
                    }
            }
            SearchResults = searchResults;
            return SearchResults;
        }

        public List<Gas> SearchByName(string name)
        {
            List<Gas> foundList;
            if (string.IsNullOrEmpty(name))
            {
                foundList = GasCollection.Values.ToList();
            }
            else
            {
                foundList = new List<Gas>();
                foreach(var element in GasCollection)
                {
                    if(element.Value.Name.ToLower().Contains(name.ToLower()))
                    {
                        foundList.Add(element.Value);
                    }
                }
            }
            this.SearchByNameResults = foundList;
            return foundList;
        }
        public bool ifOnSelected(string name)
        {
            foreach (GasSeries signal in SelectedSignals)
                {
                    if (signal.Value.getLabel.Equals(name)) return true;
                }
                return false;
        }

    }
    public class Gas
    {
        public Gas(string name)
        {
            Name = name;
            gasData = new List<GasAtTemperature>();
        }

        public string Name { set; get; }
        public List<GasAtTemperature> gasData { set; get; }

        public bool loadData(string path)
        {
            try
            {
                string filename = Path.GetFileName(path);
                string tempString = filename.Split('_', '.')[1];
                int temperature = int.Parse(tempString.Remove(tempString.Length - 1));
                int index = ifTemperatureExists(temperature);              
                List<Point> newGasData = parsePointsFromFile(path);
                if(index >= gasData.Count)
                {
                    return false;
                }
                if(index >= 0)
                {
                    //appendPoints(gasData.ElementAt(index), newGasData);
                }
                else
                {
                    gasData.Add(new GasAtTemperature(Name, newGasData.OrderBy(o => o.X).ToList(), temperature));
                }
                
                return true;
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
                return false;
            }
        }

        List<Point> parsePointsFromFile(string path)
        {
            string line = " ";
            List<Point> newPoints = new List<Point>();
            using (StreamReader sr = new StreamReader(path))
            {
                while ((line = sr.ReadLine()) != null)
                {
                    if (string.IsNullOrEmpty(line)) continue;
                    string[] pointStrings = line.Split('\t');
                    double lambda = double.Parse(pointStrings[0], System.Globalization.CultureInfo.InvariantCulture);
                    double absorpcion = double.Parse(pointStrings[1], System.Globalization.CultureInfo.InvariantCulture);
                    Point newPoint = new Point(lambda, absorpcion);
                    newPoints.Add(newPoint);
                }
            }
            return newPoints;
        }
        int ifTemperatureExists(int temperature)
        {
            int result = -1;
            int index = 0;

            foreach (GasAtTemperature singleData in gasData)
            {
                if (singleData.temperature == temperature)
                {
                    result = index;
                    break;
                }
                ++index;
            }
            return result;
        }
        void appendPoints(GasAtTemperature gasData, List<Point> points)
        {
            List<Point> newFullList = gasData.data;
            newFullList.AddRange(points);
            gasData.data = newFullList.OrderBy(o => o.X).ToList();
        }
        public GasAtTemperature getBestSignal(int temperature)
        {
            int position = -1;
            int index = 0;
            int smallestDistance = int.MaxValue;
            foreach(GasAtTemperature gas in gasData)
            {
                if(Math.Abs(gas.temperature - temperature) < smallestDistance)
                {
                    smallestDistance = Math.Abs((int)(gas.temperature - temperature));
                    position = index;
                }
                ++index;
            }
            return gasData.ElementAt(position);
        }
    }
    public class GasAtTemperature
    {
        public GasAtTemperature(string gasName, List<Point> data, float temperature)
        {
            this.GasName = gasName;
            this.data = data;
            this.temperature = temperature;
        }

        public GasAtTemperature(float temperature)
        {
            this.temperature = temperature;
            data = new List<Point>();
        }
        public List<Point> data { set; get; }
        public float temperature { set; get; }

        public readonly string GasName;
        public string getLabel { get {return GasName + " at " + temperature.ToString() + "\u2103"; }}
        public bool IfContainsPointInBox(rangeFloat absorption, range waveLengths)
        {
            double waveNumber1 = 1e7 / waveLengths.High;
            double waveNumber2 = 1e7 / waveLengths.Low;

            rangeDouble avalibleRange, searchRange;
            avalibleRange.Low = data.Min(wave => wave.X);
            avalibleRange.High = data.Max(wave => wave.X);

            searchRange.Low = Math.Max(avalibleRange.Low, waveNumber1);
            searchRange.High = Math.Min(avalibleRange.High, waveNumber2);

            int searchStartIndex = data.FindIndex(wave => wave.X >= searchRange.Low);
            int searchStopIndex = data.FindIndex(wave => wave.X >= searchRange.High);

            bool found = false;
            for(int index = searchStartIndex; index <= searchStopIndex; index += 5)
            {
                if(data.ElementAt(index).Y >= absorption.Low && data.ElementAt(index).Y <= absorption.High)
                {
                    found = true;
                    break;
                }
            }

            return found;
        }
    }
}
