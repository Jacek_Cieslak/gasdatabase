﻿using System;
using System.Windows;
using System.Windows.Forms;


namespace GasDatabase
{
    /// <summary>
    /// Logika interakcji dla klasy loadFromFileWindow.xaml
    /// </summary>
    public partial class loadFromFileWindow : Window
    {
        Database Local_db;
        string FilePath;
        string GasName;
        public loadFromFileWindow(Database db)
        {
            Local_db = db;
            InitializeComponent();

            cb_gasNames.ItemsSource = Local_db.GasCollection;

        }

        private void Tgb_gasNameSelection_Clicked(object sender, RoutedEventArgs e)
        {
            if ((bool)tgb_gasNameSelection.IsChecked)
            {
                tb_gasName.Visibility = Visibility.Hidden;
                cb_gasNames.Visibility = Visibility.Visible;
            }
            else
            {
                tb_gasName.Visibility = Visibility.Visible;
                cb_gasNames.Visibility = Visibility.Hidden;
            }
        }

        private void Pb_selectPath_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Open gas description file";
            dialog.Filter = "Text files (*.txt)|*.TXT";
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    FilePath = dialog.FileName;
                    tb_filePath.Text = FilePath;
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void Pb_cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Pb_load_Click(object sender, RoutedEventArgs e)
        {
            bool result = false;
            if(!string.IsNullOrEmpty(FilePath))
            {
                if((bool)tgb_gasNameSelection.IsChecked)
                {
                    GasName = cb_gasNames.Text;
                }
                else
                {
                    GasName = tb_gasName.Text;
                }
                if(!string.IsNullOrEmpty(GasName))
                {
                    Local_db.loadGasFromFile(FilePath, GasName);
                    Close();
                }
                else
                {
                    System.Windows.MessageBox.Show("Gas name can not be empty");
                }
            }
            else
            {
                System.Windows.MessageBox.Show("File path can not be empty");
            }
        }
    }
}
